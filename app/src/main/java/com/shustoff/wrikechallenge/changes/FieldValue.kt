package com.shustoff.wrikechallenge.changes

sealed class FieldValue {

    data class Text(val text: String) : FieldValue()

    data class Date(val date: java.util.Date) : FieldValue()
}