package com.shustoff.wrikechallenge.changes

data class LocalChanges(
    val changes: List<Change>,
    val lastSyncAt: Timestamp
)