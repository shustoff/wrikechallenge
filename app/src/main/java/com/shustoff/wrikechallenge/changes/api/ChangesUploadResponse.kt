package com.shustoff.wrikechallenge.changes.api

import com.shustoff.wrikechallenge.changes.Timestamp

sealed class ChangesUploadResponse {

    data class Success(
        val timestamp: Timestamp
    ) : ChangesUploadResponse()

    object SyncRequired : ChangesUploadResponse()
}