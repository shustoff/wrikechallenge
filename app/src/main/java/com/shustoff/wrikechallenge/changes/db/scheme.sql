CREATE TABLE entities(
    id TEXT PRIMARY KEY
);

CREATE TABLE changes(
    id INTEGER PRIMARY KEY,
    entity_id TEXT NOT NULL,
    type INTEGER NOT NULL,
    synchronized INTEGER NOT NULL,
    made_at INTEGER NOT NULL,
    FOREIGN KEY (entity_id) REFERENCES entities(id) ON DELETE CASCADE
)

CREATE TABLE fields_changes_text(
    id INTEGER PRIMARY KEY,
    field INTEGER NOT NULL, -- map int to every field type of every entity
    field_text TEXT NOT NULL,
    changes_id INTEGER NOT NULL,
    FOREIGN KEY (changes_id) REFERENCES changes(id) ON DELETE CASCADE
)

CREATE TABLE fields_changes_date(
    id INTEGER PRIMARY KEY,
    field INTEGER NOT NULL, -- map int to every field type of every entity
    field_date INTEGER NULL,
    changes_id INTEGER NOT NULL,
    FOREIGN KEY (changes_id) REFERENCES changes(id) ON DELETE CASCADE
)