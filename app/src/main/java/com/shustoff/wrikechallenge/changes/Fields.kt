package com.shustoff.wrikechallenge.changes

sealed class Fields<Value : FieldValue> {

    abstract val value: Value

    sealed class Task<Value : FieldValue> : Fields<Value>() {

        data class Title(
            override val value: FieldValue.Text
        ) : Task<FieldValue.Text>()

        data class Description(
            override val value: FieldValue.Text
        ) : Task<FieldValue.Text>()

        data class AssignedTo(
            override val value: FieldValue.Text
        ) : Task<FieldValue.Text>()

        data class Deadline(
            override val value: FieldValue.Date
        ) : Task<FieldValue.Date>()
    }
}