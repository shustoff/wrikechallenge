package com.shustoff.wrikechallenge.changes.db

import com.shustoff.wrikechallenge.changes.Entity
import com.shustoff.wrikechallenge.changes.Fields
import com.shustoff.wrikechallenge.changes.LocalChanges
import com.shustoff.wrikechallenge.changes.ServerChanges
import com.shustoff.wrikechallenge.changes.Timestamp
import java.util.Date
import kotlin.reflect.KClass

interface ChangesDao {

    suspend fun saveChanges(
        changes: ServerChanges
    )

    suspend fun markLocalChangesSynchronized(
        timestamp: Timestamp,
        lastSynchronizedLocalChangeMadeAt: Date
    )

    suspend fun getLocalChangesToSync(): LocalChanges

    suspend fun getLastSyncTimestamp(): Timestamp

    suspend fun <T : Fields<*>> update(type: KClass<T>, id: String, fields: List<T>)

    suspend fun <T : Fields<*>> create(type: KClass<T>, fields: List<T>): String

    suspend fun <T : Fields<*>> delete(type: KClass<T>, id: String)

    //TODO: use inline functions, also may return flow to subscribe for updates
    suspend fun <T : Fields<*>> getActualEntities(
        type: KClass<T>,
        fields: List<KClass<out T>>
    ): List<Entity<T>>

    suspend fun <T : Fields<*>> getActualEntity(
        type: KClass<T>,
        id: String,
        fields: List<KClass<out T>>
    ): Entity<T>?
}