package com.shustoff.wrikechallenge.changes

import kotlin.reflect.KClass

data class Entity<TFields : Fields<*>>(
    val id: String,
    val fields: Map<KClass<out TFields>, TFields>
) {

    inline fun <reified T: TFields> getField(): T? = fields[T::class] as? T

}