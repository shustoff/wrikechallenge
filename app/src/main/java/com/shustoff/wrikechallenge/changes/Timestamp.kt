package com.shustoff.wrikechallenge.changes

inline class Timestamp(val value: Long)