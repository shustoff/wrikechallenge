package com.shustoff.wrikechallenge.changes.api

import com.shustoff.wrikechallenge.changes.LocalChanges
import com.shustoff.wrikechallenge.changes.ServerChanges
import com.shustoff.wrikechallenge.changes.Timestamp

interface ChangesApi {

    suspend fun getChanges(
        after: Timestamp
    ): ServerChanges

    suspend fun uploadChanges(
        localChanges: LocalChanges
    ): ChangesUploadResponse
}