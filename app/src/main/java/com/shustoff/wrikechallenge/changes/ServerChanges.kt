package com.shustoff.wrikechallenge.changes

data class ServerChanges(
    val syncAt: Timestamp,
    val since: Timestamp,
    val changes: List<Change>
)