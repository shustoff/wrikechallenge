package com.shustoff.wrikechallenge.changes

import java.util.Date

sealed class Change {

    abstract val entityId: String
    abstract val madeAt: Date

    data class Delete(
        override val entityId: String,
        override val madeAt: Date
    ) : Change()

    data class Update(
        override val entityId: String,
        override val madeAt: Date,
        val fields: List<Fields<*>>
    ) : Change()

    data class Create(
        override val entityId: String,
        override val madeAt: Date,
        val fields: List<Fields<*>>
    ) : Change()
}