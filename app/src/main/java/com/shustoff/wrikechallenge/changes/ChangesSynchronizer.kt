package com.shustoff.wrikechallenge.changes

import com.shustoff.wrikechallenge.changes.api.ChangesUploadResponse
import com.shustoff.wrikechallenge.changes.api.ChangesApi
import com.shustoff.wrikechallenge.changes.db.ChangesDao
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel

class ChangesSynchronizer(
    private val api: ChangesApi,
    private val dao: ChangesDao
) {

    private val syncRequiredChannel = Channel<Unit>(Channel.CONFLATED)

    // call from service or app to keep it going in a background
    suspend fun startPerformingSyncWhenRequested() {
        for (i in syncRequiredChannel) {
            syncInternal()
        }
    }

    fun startSync() {
        syncRequiredChannel.offer(Unit)
    }

    fun observeStatus() : BroadcastChannel<SyncStatus> {
        TODO()
    }

    private suspend fun syncInternal() {
        // TODO: should be synchronized
        loadAndSaveServerChanges()

        val localChanges = dao.getLocalChangesToSync()

        if (localChanges.changes.isNotEmpty()) {
            val lastChangeMadeAt = localChanges.changes.maxBy { it.madeAt }?.madeAt!!
            val updateTimestamp = uploadChangesAndGetTimestamp(localChanges)
            dao.markLocalChangesSynchronized(updateTimestamp, lastChangeMadeAt)
        }
    }

    private suspend fun loadAndSaveServerChanges() {
        val lastSync = dao.getLastSyncTimestamp()
        val loadedChanges = api.getChanges(lastSync)
        dao.saveChanges(loadedChanges)
    }

    private suspend fun uploadChangesAndGetTimestamp(changes: LocalChanges): Timestamp {
        val uploadResult = api.uploadChanges(changes)

        return when (uploadResult) {
            is ChangesUploadResponse.Success -> uploadResult.timestamp
            ChangesUploadResponse.SyncRequired ->
                TODO("load data from server here and try again. Throw error if still not saved")
        }
    }

    sealed class SyncStatus {

        object InProgress : SyncStatus()

        object Idle : SyncStatus()

        data class Error(
            val reason: SyncFailReason
        )
    }

    sealed class SyncFailReason {
        object ServiceUnavailable : SyncFailReason()

        data class ApiError(
            val message: String
        ) : SyncFailReason()
    }
}