package com.shustoff.wrikechallenge.tasks

import java.util.Date

data class TaskDetails(
    val title: String,
    val assignedTo: String,
    val deadline: Date?,
    val description: String
)