package com.shustoff.wrikechallenge.tasks

import java.util.Date

data class TaskShortInfo(
    val id: String,
    val title: String,
    val assignedTo: String,
    val deadline: Date?
)