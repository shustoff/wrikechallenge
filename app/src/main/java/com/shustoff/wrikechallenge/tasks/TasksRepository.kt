package com.shustoff.wrikechallenge.tasks

import com.shustoff.wrikechallenge.changes.Entity
import com.shustoff.wrikechallenge.changes.FieldValue
import com.shustoff.wrikechallenge.changes.Fields.Task
import com.shustoff.wrikechallenge.changes.db.ChangesDao
import java.util.Date
import kotlin.reflect.KClass

class TasksRepository(
    private val dao: ChangesDao
) {

    private val shortInfoFields = listOf<KClass<out Task<*>>>(
        Task.Title::class,
        Task.AssignedTo::class,
        Task.Deadline::class
    )

    private val fullInfoFields = shortInfoFields +
        listOf<KClass<out Task<*>>>(
            Task.Description::class
        )

    suspend fun getTasks(): List<TaskShortInfo> {
        return dao.getActualEntities(Task::class, shortInfoFields)
            .map { entity ->
                TaskShortInfo(
                    id = entity.id,
                    title = entity.text<Task.Title>(),
                    assignedTo = entity.text<Task.AssignedTo>(),
                    deadline = entity.date<Task.Deadline>()
                )
            }
    }

    suspend fun getTask(id: String): TaskDetails? {
        return dao.getActualEntity(Task::class, id, fullInfoFields)
            ?.let { entity ->
                TaskDetails(
                    title = entity.text<Task.Title>(),
                    assignedTo = entity.text<Task.AssignedTo>(),
                    deadline = entity.date<Task.Deadline>(),
                    description = entity.text<Task.Description>()
                )
            }
    }

    suspend fun updateTask(id: String, task: TaskDetails) {
        dao.update(
            type = Task::class,
            id = id,
            fields = listOfNotNull<Task<*>>(
                Task.Title(FieldValue.Text(task.title)),
                Task.Description(FieldValue.Text(task.description)),
                Task.AssignedTo(FieldValue.Text(task.assignedTo)),
                task.deadline?.let { Task.Deadline(FieldValue.Date(it)) }
            )
        )
    }

    suspend fun createTask(task: TaskDetails): String {
        return dao.create(
            type = Task::class,
            fields = listOfNotNull<Task<*>>(
                Task.Title(FieldValue.Text(task.title)),
                Task.Description(FieldValue.Text(task.description)),
                Task.AssignedTo(FieldValue.Text(task.assignedTo)),
                task.deadline?.let { Task.Deadline(FieldValue.Date(it)) }
            )
        )
    }

    private inline fun <reified T : Task<FieldValue.Text>> Entity<Task<*>>.text(): String =
        getField<T>()?.value?.text.orEmpty()

    private inline fun <reified T : Task<FieldValue.Date>> Entity<Task<*>>.date(): Date? =
        getField<T>()?.value?.date
}